declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}

declare module 'firebase'
declare module 'firebase/app'
