import Vue from 'vue'
import Router from 'vue-router'
import Home from '../views/Home.vue'
import { myStore } from '@/store'

Vue.use(Router)

export const router: Router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home,
      beforeEnter: (to: any, from: any, next: any) => {
        // ..
        console.log('route leave')
      }
    }
  ]
})

router.afterEach((to, from) => {
  myStore.LeavePage()
  console.log('page leave')
})
