import Vue from 'vue'
import Vuex from 'vuex'
import { fb, fbData } from './database'
// import { vm } from '@/main'

Vue.use(Vuex)

export type StoreGame = {
  key: string; // host user
}

export type StoreState = {
  username: string;
  logged: boolean;
  update: boolean;
}

export type StoreUser = {
  key: string;
  name: string;
  points: number;
  actions: number;
  posts: object[];
  active: boolean;
  ingame: boolean;
  host: boolean;
}

export default class StoreClass {
  public state: StoreState = {
    username: '',
    logged: false,
    update: false
  }

  public userList: StoreUser[] = []
  public userData: StoreUser = {
    key: '',
    name: '',
    points: 0,
    actions: 0,
    posts: [],
    active: false,
    ingame: false,
    host: false
  }

  public game: StoreGame = {
    key: ''
  }

  public gameUser!: StoreUser

  public Init () {
    fb.login()
  }

  public onLogin (data: fbData) {
    console.log('Login complete ' + data.email)
    this.state.username = data.email
    this.state.logged = true
    this.userList = []
    fb.StartListen()
  }

  public onUserChange (data: fbData) {
    const user: StoreUser = {
      key: data.key,
      name: data.val().name,
      points: data.val().points,
      actions: data.val().actions,
      posts: data.val().posts ? data.val().posts : [],
      active: data.val().active,
      ingame: data.val().ingame,
      host: data.val().host
    }

    this.userList.push(user)
  }

  public onSignal (snap: fbData) {
    this.userList.forEach(element => {
      if (element.key === snap.key) {
        element.active = snap.val().active
        element.ingame = snap.val().ingame
      }
    })
  }

  public onRemoveUser (snap: fbData) {
    console.log(snap.val())
    let del = -1
    let idx = 0
    this.userList.forEach(element => {
      if (element.key === snap.key) {
        del = idx
      }
      idx++
    })
    if (del > -1) {
      this.userList.splice(del, 1)
    }
  }

  public StartGame () {
    this.userData.ingame = true
    fb.UpdateUser(this.userData)
    fb.SendSignal(this.userData)
    if (this.game.key === '') {
      this.game.key = this.userData.key
      fb.UdpdateGame()
    }
  }

  public onGameSignal (snap: fbData) {
    this.game.key = snap.key
    this.gameUser = this.GetUserByKey(this.game.key)

    console.log('Setting key')
  }

  public AddUser (name: string) {
    fb.AddUser(name)
  }

  public onUserAdd () {
    // ..
  }

  public CalcPostPoints () {
    let points = 0
    if (!this.userData.posts) {
      this.userData.posts = []
    }
    this.userData.posts.forEach(function (obj: any) {
      points += Number(obj.points)
    })
    this.userData.actions = points
  }

  public PostAdd (text: string, points: number) {
    if (!this.userData.posts) {
      this.userData.posts = []
    }
    this.userData.posts.push({ text: text, points: points })
    this.CalcPostPoints()
    fb.UpdateUser(this.userData)
  }

  public RemovePost (n: number) {
    if (!this.userData.posts) {
      this.userData.posts = []
    }
    this.userData.posts.splice(n, 1)
    this.CalcPostPoints()
    fb.UpdateUser(this.userData)
  }

  public LeavePage () {
    if (this.userData.key !== '') {
      fb.LeavePage()
    }
  }

  public GetUserByKey (key: string): StoreUser {
    this.userList.forEach(element => {
      if (element.key === key) {
        return element
      }
    })
    return {
      key: '',
      name: '',
      points: 0,
      actions: 0,
      posts: [],
      active: false,
      ingame: false,
      host: false
    }
  }
}
export const myStore: StoreClass = new StoreClass()
