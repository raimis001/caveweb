import { myStore, StoreUser } from '../store'
import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/database'
import 'firebase/auth'
import { firebaseConfig } from './conf/fbConfig'

firebase.initializeApp(firebaseConfig)
type fbError = any // firebase.auth.Error
type fbAuth = any // firebase.auth.Auth
export type fbData = any // firebase.database.DataSnapshot

const userRef = firebase.database().ref('users')
const signalsRef = firebase.database().ref('signals')
const gameRef = firebase.database().ref('game')

export default class Database {
  public login () {
    firebase.auth()
      .signInWithEmailAndPassword(firebaseConfig.email, firebaseConfig.password)
      .catch(function (error: fbError) {
        console.log(error.code)
        console.log(error.message)
      })
      .then(function (auth: fbAuth) {
        myStore.onLogin(auth.user)
      })
  }

  public StartListen () {
    userRef.on('child_added', function (snap: fbData) {
      myStore.onUserChange(snap)
    })
    userRef.on('child_removed', function (snap: fbData) {
      myStore.onRemoveUser(snap)
    })
    signalsRef.on('value', function (snap: fbData) {
      snap.forEach(function (child: fbData) {
        myStore.onSignal(child)
      })
    })
    gameRef.on('value', function (snap: fbData) {
      snap.forEach(function (child: fbData) {
        myStore.onGameSignal(child)
      })
    })
  }

  public LeavePage () {
    myStore.userData.active = false
    myStore.userData.ingame = false
    this.UpdateUser(myStore.userData)
    this.SendSignal(myStore.userData)
    console.log('leav page')
  }

  public AddUser (user: string) {
    firebase.database().ref('users/' + user.toUpperCase())
      .once('value', function (data: fbData) {
        if (data.val()) {
          console.log('Find user: ' + data.key)

          myStore.userData.key = data.key
          myStore.userData.name = data.val().name
          myStore.userData.points = data.val().points
          myStore.userData.posts = data.val().posts ? data.val().posts : []
          myStore.userData.actions = data.val().actions ? data.val().actions : 0
          myStore.userData.active = true
          myStore.userData.ingame = false
          myStore.userData.host = false

          myStore.onUserAdd()
        } else {
          console.log('Creating new user')

          myStore.userData.key = user.toUpperCase()
          myStore.userData.name = user
          myStore.userData.active = true

          const updates = {
            [myStore.userData.key]: myStore.userData
          }

          userRef.update(updates)
            .then(function () {
              myStore.onUserAdd()
            })
        }
        firebase.database().ref('signals/' + user.toUpperCase())
          .update({ active: true, ingame: false })
      })
  }

  public SendSignal (user: StoreUser) {
    firebase.database().ref('signals/' + user.key)
      .update({ active: user.active, ingame: user.ingame })
  }

  public UpdateUser (user: StoreUser) {
    if (user.key) {
      const updates = {
        [user.key]: user
      }

      userRef.update(updates)
    }
  }

  public UdpdateGame () {
    gameRef.update(myStore.game)
  }
}

export const fb: Database = new Database()
